import BaseHeader from "@/components/baseHeader/index.vue"
import { App } from "vue"

export default {
  install(app: App): void {
    app.component("base-header", BaseHeader)
  },
}
