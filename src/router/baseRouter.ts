const baseRouter = [
  {
    path: "/article",
    name: "article",
    component: () => import("@/views/Article.vue"),
    meta: {
      title: "文章",
      nav: true,
    },
  },
  {
    path: "/archive",
    name: "archive",
    component: () =>
      import(/* webpackChunkName: "archive" */ "@/views/Archive.vue"),
    meta: {
      title: "归档",
      nav: true,
    },
  },
  {
    path: "/kbrochure",
    name: "kbrochure",
    component: () =>
      import(/* webpackChunkName: "kbrochure" */ "@/views/Kbrochure.vue"),
    meta: {
      title: "知识小册",
      nav: true,
    },
  },
  {
    path: "/detail",
    name: "detail",
    component: () =>
      import(/* webpackChunkName: "detail" */ "@/views/Detail.vue"),
    meta: {
      title: "详情",
      nav: false,
    },
  },
  {
    path: "/about",
    name: "about",
    component: () =>
      import(/* webpackChunkName: "about" */ "@/views/About.vue"),
  },
  {
    path: "/search",
    name: "search",
    component: () =>
      import(/* webpackChunkName: "search" */ "@/views/Search.vue"),
  },
  {
    path: "/page/:id",
    component: () => import("@/views/page/index.vue"),
    meta: {
      nav: false,
    },
  },
  {
    path: "/",
    redirect: "/article",
  },
]

export default baseRouter
