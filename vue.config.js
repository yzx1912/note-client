const path = require("path")

module.exports = {
  lintOnSave: false, // 关闭组件命名规则
  chainWebpack: (config) => {
    const types = ["vue-modules", "vue", "normal-modules", "normal"]
    types.forEach((type) =>
      addStyleResource(config.module.rule("less").oneOf(type))
    ),
      // 更改html的title
      config.plugin("html").tap((args) => {
        args[0].title = "八维创作平台"
        return args
      })
  },
  devServer: {
    proxy: {
      "/api": {
        target: "https://creationapi.shbwyz.com",
        ws: true,
        changeOrigin: true,
      },
    },
  },
}

function addStyleResource(rule) {
  rule
    .use("style-resource")
    .loader("style-resources-loader")
    .options({
      patterns: [path.resolve(__dirname, "./src/styles/var.less")],
    })
}
